from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientRepository:
    def __init__(self):
        self.db = {}
        self.next_patient_id = 1

    def add_patient(self, patient: Patient):
        # Generate a unique patient ID
        patient_id = self.next_patient_id
        self.next_patient_id += 1

        # Create a PatientEntity with ID
        patient_entity = PatientEntity(
            id=patient_id,
            first_name=patient.first_name,
            last_name=patient.last_name,
            date_of_birth=patient.date_of_birth,
            social_security_number=patient.social_security_number
        )

        # Store the patient entity in the database
        self.db[patient_id] = patient_entity

        # Return the created patient entity
        return patient_entity

    def get_patient(self, patient_id):
        return self.db.get(patient_id)
    
    def get_patient_by_social_security_number(self, social_security_number):
        for patient_id, patient_entity in self.db.items():
            if patient_entity.social_security_number == social_security_number:
                return patient_entity.to_dict()

        return None
    
    def get_patients_by_name(self, name):
        matching_patients = []

        for patient_id, patient_entity in self.db.items():
            full_name = f"{patient_entity.first_name} {patient_entity.last_name}"
            if name.lower() in full_name.lower():
                matching_patients.append(patient_entity.to_dict())

        return matching_patients
    
    def get_patients_by_surname(self, surname):
        matching_patients = []

        for patient_id, patient_entity in self.db.items():
            if patient_entity.last_name.lower() == surname.lower():
                matching_patients.append(patient_entity.to_dict())

        return matching_patients
    
    def update_patient(self, patient_id, updated_patient_data):
        if patient_id not in self.db:
            return None 

        patient_entity = self.db[patient_id]

        for key, value in updated_patient_data.items():
            setattr(patient_entity, key, value)

        return patient_entity.to_dict()
    
    def delete_patient(self, patient_id):
        if patient_id not in self.db:
            return None 

        deleted_patient = self.db.pop(patient_id)
        return deleted_patient.to_dict()
    
    def get_patient_by_id(self, patient_id):
        patient_entity = self.db.get(patient_id)
        if patient_entity:
            return patient_entity.to_dict()
        else:
            return None


    

    

    

    
    