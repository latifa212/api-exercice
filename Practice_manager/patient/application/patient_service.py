from datetime import datetime

from patient.domain.patient import Patient

class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient_data):
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']
        if any(not patient_data.get(field) for field in required_fields):
            raise ValueError('Patient first name is required')
        if patient_data['date_of_birth'] != datetime.strptime(patient_data['date_of_birth'], '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')
        # patient = Patient(**patient_data)
        created_patient = self.patient_repository.add_patient(patient_data)
        return created_patient


    def search_patient_by_social_security_number(self, social_security_number):
        if not social_security_number:
            raise ValueError('Social Security Number is required for searching a patient.')
        
        found_patient = self.patient_repository.get_patient_by_social_security_number(social_security_number)

        if not found_patient:
            raise ValueError(f'Patient with Social Security Number {social_security_number} not found.')

        return found_patient

    def search_patient_by_name(self, name):
        if not name:
            raise ValueError('Name is required for searching a patient.')

        found_patients = self.patient_repository.get_patients_by_name(name)

        if not found_patients:
            raise ValueError(f'No patients found with the name {name}.')

        return found_patients
    
    def search_patient_by_surname(self, surname):
        if not surname:
            raise ValueError('Surname is required for searching a patient.')

        found_patients = self.patient_repository.get_patients_by_surname(surname)

        if not found_patients:
            raise ValueError(f'No patients found with the surname {surname}.')

        return found_patients
    
    def update_patient(self, patient_id, updated_patient_data):
        if not patient_id or not updated_patient_data:
            raise ValueError('Patient ID and updated data are required for updating a patient.')

        updated_patient = self.patient_repository.update_patient(patient_id, updated_patient_data)

        if not updated_patient:
            raise ValueError(f'Patient with ID {patient_id} not found.')

        return updated_patient
    
    def delete_patient(self, patient_id):
        if not patient_id:
            raise ValueError('Patient ID is required for deleting a patient.')

        deleted_patient = self.patient_repository.delete_patient(patient_id)

        if not deleted_patient:
            raise ValueError(f'Patient with ID {patient_id} not found.')

        return deleted_patient
    
    def get_patient_by_id(self, patient_id):
        if not patient_id:
            raise ValueError('Patient ID is required for getting a patient.')

        found_patient = self.patient_repository.get_patient_by_id(patient_id)

        if not found_patient:
            raise ValueError(f'Patient with ID {patient_id} not found.')

        return found_patient
    
    
