
from flask import request, jsonify, Blueprint
from flask import current_app as app

from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)

@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/search-by-social-security-number', methods=['GET'])
def search_patient_by_social_security_number():
    try:
        social_security_number = request.args.get('social_security_number')
        
        if not social_security_number:
            raise ValueError('Social Security Number is required for searching a patient.')

        patient_service = app.config['patient_service']
        found_patient = patient_service.search_patient_by_social_security_number(social_security_number)

        return jsonify(found_patient), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/search-by-name', methods=['GET'])
def search_patient_by_name():
    try:
        name = request.args.get('name')

        if not name:
            raise ValueError('Name is required for searching a patient.')

        patient_service = app.config['patient_service']
        found_patients = patient_service.search_patient_by_name(name)

        return jsonify(found_patients), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/search-by-surname', methods=['GET'])
def search_patient_by_surname():
    try:
        surname = request.args.get('surname')

        if not surname:
            raise ValueError('Surname is required for searching a patient.')

        patient_service = app.config['patient_service']
        found_patients = patient_service.search_patient_by_surname(surname)

        return jsonify(found_patients), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/update', methods=['PUT'])
def update_patient():
    try:
        patient_id = request.args.get('patient_id')
        updated_patient_data = request.get_json()

        if not patient_id or not updated_patient_data:
            raise ValueError('Patient ID and updated data are required for updating a patient.')

        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id, updated_patient_data)

        return jsonify(updated_patient), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/delete', methods=['DELETE'])
def delete_patient():
    try:
        patient_id = request.args.get('patient_id')

        if not patient_id:
            raise ValueError('Patient ID is required for deleting a patient.')

        patient_service = app.config['patient_service']
        deleted_patient = patient_service.delete_patient(patient_id)

        return jsonify(deleted_patient), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient_by_id(patient_id):
    try:
        if not patient_id:
            raise ValueError('Patient ID is required for getting a patient.')

        patient_service = app.config['patient_service']
        found_patient = patient_service.get_patient_by_id(patient_id)

        return jsonify(found_patient), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400
