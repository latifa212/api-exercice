import unittest
from unittest.mock import Mock
from flask.testing import FlaskClient

from app import create_app
from patient.application.patient_service import PatientService


class TestPatientResource(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service

    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_service.create_patient.return_value = patient_data

        response = self.client.post('/patients', json=patient_data)

        self.assertEqual(response.status_code, 200) 
        self.assertEqual(response.json, patient_data)
        self.mock_service.create_patient.assert_called_once()

    def test_search_patient_by_social_security_number(self):
        social_security_number = '123456789012345'
        expected_patient_data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': social_security_number
        }

        self.mock_service.search_patient_by_social_security_number.return_value = expected_patient_data

        response = self.client.get(f'/patients/{social_security_number}', headers={'Authorization': 'Bearer YOUR_ACCESS_TOKEN'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_patient_data)
        self.mock_service.search_patient_by_social_security_number.assert_called_once_with(social_security_number)

    def test_search_patient_by_social_security_number_nonexistent(self):
        social_security_number = 'Nonexistent'
        self.mock_service.search_patient_by_social_security_number.side_effect = ValueError(f'Patient not found with SSN: {social_security_number}')

        response = self.client.get(f'/patients/{social_security_number}', headers={'Authorization': 'Bearer YOUR_ACCESS_TOKEN'})

        self.assertEqual(response.status_code, 404)
        self.mock_service.search_patient_by_social_security_number.assert_called_once_with(social_security_number)

    def test_search_patient_by_name(self):
        name = 'John Doe'
        expected_patient_data = [
            {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'date_of_birth': '1980-01-01',
                'social_security_number': '123456789012345'
            }
        ]

        self.mock_service.search_patient_by_name.return_value = expected_patient_data

        response = self.client.get(f'/patients?name={name}')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_patient_data)
        self.mock_service.search_patient_by_name.assert_called_once_with(name)

    def test_search_patient_by_name_nonexistent(self):
        name = 'Nonexistent'
        self.mock_service.search_patient_by_name.side_effect = ValueError(f'No patients found with the name: {name}')

        response = self.client.get(f'/patients?name={name}')

        self.assertEqual(response.status_code, 400)
        self.mock_service.search_patient_by_name.assert_called_once_with(name)

    def test_search_patient_by_surname(self):
        surname = 'Doe'
        expected_patient_data = [
            {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'date_of_birth': '1980-01-01',
                'social_security_number': '123456789012345'
            }
        ]

        self.mock_service.search_patient_by_surname.return_value = expected_patient_data

        response = self.client.get(f'/patients?surname={surname}')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_patient_data)
        self.mock_service.search_patient_by_surname.assert_called_once_with(surname)

    def test_search_patient_by_surname_nonexistent(self):
        surname = 'Nonexistent'
        self.mock_service.search_patient_by_surname.side_effect = ValueError(f'No patients found with the surname: {surname}')

        response = self.client.get(f'/patients?surname={surname}')

        self.assertEqual(response.status_code, 400)
        self.mock_service.search_patient_by_surname.assert_called_once_with(surname)
    
    def test_update_patient(self):
        patient_id = 1
        updated_patient_data = {
            'first_name': 'UpdatedJohn',
            'last_name': 'UpdatedDoe',
            'date_of_birth': '1990-01-01',
            'social_security_number': '987654321098765'
        }
        expected_updated_patient = {
            'id': patient_id,
            **updated_patient_data
        }

        self.mock_service.update_patient.return_value = expected_updated_patient

        response = self.client.put(f'/patients/{patient_id}', json=updated_patient_data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_updated_patient)
        self.mock_service.update_patient.assert_called_once_with(patient_id, updated_patient_data)

    def test_update_patient_nonexistent(self):
        patient_id = 999
        updated_patient_data = {
            'first_name': 'UpdatedJohn',
            'last_name': 'UpdatedDoe',
            'date_of_birth': '1990-01-01',
            'social_security_number': '987654321098765'
        }
        self.mock_service.update_patient.side_effect = ValueError(f'Patient not found with ID: {patient_id}')

        response = self.client.put(f'/patients/{patient_id}', json=updated_patient_data)

        self.assertEqual(response.status_code, 400)
        self.mock_service.update_patient.assert_called_once_with(patient_id, updated_patient_data)

    def test_delete_patient(self):
        patient_id = 1
        expected_deleted_patient = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_service.delete_patient.return_value = expected_deleted_patient

        response = self.client.delete(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_deleted_patient)
        self.mock_service.delete_patient.assert_called_once_with(patient_id)

    def test_delete_patient_nonexistent(self):
        patient_id = 999
        self.mock_service.delete_patient.side_effect = ValueError(f'Patient not found with ID: {patient_id}')

        response = self.client.delete(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 400)
        self.mock_service.delete_patient.assert_called_once_with(patient_id)

    def test_get_patient_by_id(self):
        patient_id = 1
        expected_patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_service.get_patient_by_id.return_value = expected_patient_data

        response = self.client.get(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_patient_data)
        self.mock_service.get_patient_by_id.assert_called_once_with(patient_id)

    def test_get_patient_by_id_nonexistent(self):
        patient_id = 999
        self.mock_service.get_patient_by_id.side_effect = ValueError(f'Patient not found with ID: {patient_id}')

        response = self.client.get(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 400)
        self.mock_service.get_patient_by_id.assert_called_once_with(patient_id)
    
