import unittest
from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity
from patient.infrastructure.patient_repository import PatientRepository


class TestPatientRepository(unittest.TestCase):
    def setUp(self):
        self.repository = PatientRepository()

    def test_add_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        self.assertEqual(self.repository.get_patient(new_patient.id), PatientEntity(**{'id': 1, **patient_data}))

    def test_get_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient(new_patient.id)

        self.assertEqual(retrieved_patient, PatientEntity(**{'id': 1, **patient_data}))
    
    def test_get_patient_by_social_security_number(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient_by_social_security_number('123456789012345')

        expected_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        self.assertEqual(retrieved_patient, expected_patient_data)

        
    def test_get_patient_by_social_security_number_nonexistent(self):
        retrieved_patient = self.repository.get_patient_by_social_security_number('999999999999999')

        self.assertIsNone(retrieved_patient)
    
    def test_search_patient_by_name(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        self.repository.add_patient(patient)

        retrieved_patients = self.repository.get_patients_by_name('John')

        self.assertEqual(len(retrieved_patients), 1)
        self.assertEqual(retrieved_patients, patient_data)
        
    def test_search_patient_by_name_nonexistent(self):
        retrieved_patients = self.repository.get_patients_by_name('Nonexistent')

        self.assertEqual(len(retrieved_patients), 0)
    
    def test_search_patient_by_surname(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        self.repository.add_patient(patient)

        retrieved_patients = self.repository.get_patients_by_surname('Doe')

        self.assertEqual(len(retrieved_patients), 1)
        self.assertEqual(retrieved_patients, patient_data)

    def test_search_patient_by_surname_nonexistent(self):
        retrieved_patients = self.repository.get_patients_by_surname('Nonexistent')

        self.assertEqual(len(retrieved_patients), 0)
    
    def test_update_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        updated_data = {
            'first_name': 'Jane',
            'last_name': 'Smith',
            'date_of_birth': '1990-02-02',
            'social_security_number': '987654321098765'
        }

        updated_patient = self.repository.update_patient(new_patient.id, updated_data)

        self.assertEqual(updated_patient['first_name'], updated_data['first_name'])
        self.assertEqual(updated_patient['last_name'], updated_data['last_name'])
        self.assertEqual(updated_patient['date_of_birth'], updated_data['date_of_birth'])
        self.assertEqual(updated_patient['social_security_number'], updated_data['social_security_number'])
        

    def test_update_patient_nonexistent(self):
        updated_patient = self.repository.update_patient(999, {})
        self.assertIsNone(updated_patient)
    
    def test_delete_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        deleted_patient = self.repository.delete_patient(new_patient.id)
        self.assertEqual(deleted_patient['first_name'], patient_data['first_name'])
        self.assertEqual(deleted_patient['last_name'], patient_data['last_name'])
        self.assertEqual(deleted_patient['date_of_birth'], patient_data['date_of_birth'])
        self.assertEqual(deleted_patient['social_security_number'], patient_data['social_security_number'])

    def test_delete_patient_nonexistent(self):
        deleted_patient = self.repository.delete_patient(999)
        self.assertIsNone(deleted_patient)

    def test_get_patient_by_id(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient_by_id(new_patient.id)

        self.assertEqual(retrieved_patient['first_name'], patient_data['first_name'])
        self.assertEqual(retrieved_patient['last_name'], patient_data['last_name'])
        self.assertEqual(retrieved_patient['date_of_birth'], patient_data['date_of_birth'])
        self.assertEqual(retrieved_patient['social_security_number'], patient_data['social_security_number'])

    def test_get_patient_by_id_nonexistent(self):
        retrieved_patient = self.repository.get_patient_by_id(999)

        self.assertIsNone(retrieved_patient)