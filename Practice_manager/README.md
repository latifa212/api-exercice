As a doctor Thomas
I want to keep records to my patients
So that I can access them later

Acceptance criteria / Acceptance tests

Scenario : Create a patient
Given I an a doctor Thomas
When i create a patient John Doe with the Social Security Number 1 36 57 637 278 977
Then I should see the patient in the list of patients

Scenario : Search a patient by Social Security Number
Given I am a doctor Thomas
And I have a patient John Doe with the Social Security Number 1 36 57 637 278 977
When I search for the patient with the Social Security Number 1 36 57 637 278 977
Then I sould see the patient John Doe in the list of patients

Scenario : Search a patient by name
Given I am a doctor Thomas
And I have a patient John Doe with the Social Security Number 1 36 57 637 278 977
When I search for the patient with the name John Doe
Then I should see the patient John Doe in the list of patients

Scenario : Search a patient by surname
Given I am a doctor Thomas
And I have a patient John Doe with the Social Security Number 1 36 57 637 278 977
When I search for the patient with the surname Doe
Then I should see the patient John Doe in the list of patients

Scenario: Update a patient
Given I am a doctor Thomas
And I have a patient John Doe with the social security number 1 36 57 637 278 977
When I update the patient John Doe with the social security number 1 63 54 854 471 725
Then I should see the patient John Doe in the list of patients
And his social security number should be 1 63 54 854 471 725

Scenario: Delete a patient
Given I am a doctor Thomas
And I have a patient John Doe with the social security number 1 63 54 854 471 725
When I delete the patient John Doe
Then I should not see the patient John Doe in the list of patients

Scenario: Get a patient by ID
Given I am a doctor Thomas
And I have a patient John Doe with the ID 1
When I get the patient with the ID 1
Then I should see the patient John Doe informations
